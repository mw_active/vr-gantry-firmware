# FIRMWARE - HTC Vive Gantry #

![](https://cdn-images-1.medium.com/max/800/1*L0-0M3ktUiBQcZHqOwzkyA.gif)

Stepper motor control for gantry head positioning based on HTC Vive HMD coordinates.

### Configuration ###

* Board: Teensy 3.2 
* Dependencies: AccelStepper Library 1.51
* IDE: Arduino

### Dependencies ###
[Software Repository](https://github.com/MistyWestAdmin/VR-Gantry-Software)<br>
[Mechanical Repository](https://github.com/MistyWestAdmin/VR-Gantry-Mechanical)

### Contact ###

* Repo owner: Justin / Mistywest
* Contributors: Div
* [Mistywest](https://mistywest.com/)
<br><br>
![](https://mistywest.com/wp-content/uploads/2015/11/logo_sticky.png)
<br>
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
